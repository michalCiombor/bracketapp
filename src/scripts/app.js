import '../styles/scss/style.scss';
import Test from "./test.js";

import domReady from "./dom-ready.js";

domReady(() => {
  Test.init();
});
