const testConfig = {
  className: {
    BracketForm: "bracketForm",
    BracketFormButton: "bracketForm_formButton",
    BracketFormInput: "bracketForm_formInput",
    BracketFormResponse: "bracketForm_formResponse"
  }
};

const Test = (() => {
  const { className } = testConfig;
  const TEST_FORM = document.querySelector(`.${className.BracketForm}`);
  const FORM_INPUT = document.querySelector(`.${className.BracketFormInput}`);
  const FORM_BUTTON = document.querySelector(`.${className.BracketFormButton}`);
  const FORM_RESPONSE = document.querySelector(
    `.${className.BracketFormResponse}`
  );

  const handleSuccess = data => {
    const resp = JSON.parse(data);
    const date = resp[0].data;
    const rate = resp[0].cena;

    FORM_RESPONSE.innerText = `Gold rate as of ${date}: ${rate} zł`;
  };

  const handleFailure = () => {
    FORM_RESPONSE.innerText = "IT IS NOT A TIME MACHINE!";
    throw "HUSTON WE A HAVE A SITUATION";
  };

  const handleNoData = () => {
    FORM_RESPONSE.innerText = "PLEASE SELECT DATE ABOVE";
  };

  const handleRequest = () => {
    const date = FORM_INPUT.value;

    if (date.length > 0) {
      // eslint-disable-next-line no-undef
      const request = new XMLHttpRequest();
      const requestEndpoint = `http://api.nbp.pl/api/cenyzlota/${date}?format=json`;
      request.open("GET", `${requestEndpoint}`, true);
      request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
          handleSuccess(this.response);
        } else {
          handleFailure();
        }
      };

      request.onerror = function() {
        throw `Huston we have a problem!`;
      };

      request.send();
    } else {
      handleNoData();
    }
  };

  const handleSubmit = event => {
    event.preventDefault();
    handleRequest();
  };
  const init = () => {
    if (TEST_FORM) {
      FORM_BUTTON.addEventListener("click", handleSubmit);
    }
  };

  return {
    init
  };
})();

export default Test;
